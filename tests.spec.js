const octetCalculator = require('./index')()

assert(octetCalculator.fromOctet('00000000'), 0)
assert(octetCalculator.fromOctet('00000001'), 1)
assert(octetCalculator.fromOctet('00000011'), 3)
assert(octetCalculator.fromOctet('00010000'), 16)
assert(octetCalculator.fromOctet('00010100'), 20)
assert(octetCalculator.fromOctet('00011100'), 28)
assert(octetCalculator.fromOctet('01010100'), 84)
assert(octetCalculator.fromOctet('10000000'), 128)
assert(octetCalculator.fromOctet('10001000'), 136)
assert(octetCalculator.fromOctet('11111111'), 255)

assert(octetCalculator.toOctet(0), '00000000')
assert(octetCalculator.toOctet(1), '00000001')
assert(octetCalculator.toOctet(3), '00000011')
assert(octetCalculator.toOctet(4), '00000100')
assert(octetCalculator.toOctet(6), '00000110')
assert(octetCalculator.toOctet(16), '00010000')
assert(octetCalculator.toOctet(33), '00100001')
assert(octetCalculator.toOctet(68), '01000100')
assert(octetCalculator.toOctet(128), '10000000')
assert(octetCalculator.toOctet(132), '10000100')
assert(octetCalculator.toOctet(129), '10000001')

function assert(expect, equal) {
    if (expect === equal) {
        console.log('---Test passed---')
    } else {
        console.log('expect: ', expect, ' to equal: ', equal)
        throw new Error('_____Test not working_____')
    }
}
