function OctetCalculator() {
    return { toOctet, fromOctet }
}

function toOctet(input) {
    const BASE_BINARY_OCTET = '00000000'
    const BINARY_TRUE = '1'
    const OCTET_TABLE = [128, 64, 32, 16, 8, 4, 2, 1]
    return OCTET_TABLE
        .reduce((accumulator, item, index, arr) => {
            if (arr[index] <= input) {
                input = input - OCTET_TABLE[index]
                accumulator = accumulator.substr(0, index) + BINARY_TRUE + accumulator.substr(index + 1)
            }
            return accumulator
        }, BASE_BINARY_OCTET)
}

function fromOctet(input) {
    const BINARY_TRUE = '1'
    const OCTET_TABLE = [128, 64, 32, 16, 8, 4, 2, 1]
    return input
        .split('')
        .reduce((accumulator, item, index) => {
            return item === BINARY_TRUE ? accumulator + OCTET_TABLE[index] : accumulator
        }, 0)
}

module.exports = OctetCalculator
